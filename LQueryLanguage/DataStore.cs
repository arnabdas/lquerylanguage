﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LQueryLanguage
{
    public class DataStore
    {
        public void GetProducts()
        {
            using (SqlConnection con = new SqlConnection(Properties.Settings.Default.NorthwindConnection))
            {
                try
                {
                    con.Open();
                    try
                    {
                        using (SqlCommand cmd = new SqlCommand("SELECT * FROM [DBO].[PRODUCTS]", con))
                        {
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    $"{reader[1]} \t | {reader[5]}".LogInfo();
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        $"Could not connect to the database{Environment.NewLine}{ex.Message}".LogError();
                    }
                    con.Close();
                }
                catch (Exception ex)
                {
                    $"Could not connect to the database{Environment.NewLine}{ex.Message}".LogError();
                }
            }
        }
    }
}
