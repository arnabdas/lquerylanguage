﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LQueryLanguage
{
    public static class RegexHelpers
    {
        static Regex _alph = new Regex("[a-z0-9<=>.]", RegexOptions.IgnoreCase);
        static Regex _whSpce = new Regex(@"\s", RegexOptions.Compiled);

        public static bool IsAlphaNumericOrOperator(this string s)
        {
            return _alph.IsMatch(s);
        }

        public static bool IsWhiteSpace(this string s)
        {
            return _whSpce.IsMatch(s);
        }

        public static bool IsPunctuations(this string s)
        {
            return "(),;{}".IndexOf(s) > -1;
        }

        public static bool IsOperators(this string s)
        {
            return "<=>".IndexOf(s) > -1;
        }

        public static bool IsQuotes(this string s)
        {
            return "'".IndexOf(s) > -1;
        }
    }
}
