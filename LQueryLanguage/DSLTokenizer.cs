﻿using Microsoft.CSharp;
using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace LQueryLanguage
{
    public class DSLTokenizer
    {
        // 
        public static void Process()
        {
            //ProcessQueryFile();

            var source = GetSource(GetSourceGraph());
        }

        public static string GetSource(CodeCompileUnit compileUnit)
        {
            StringBuilder sb = new StringBuilder();

            try
            {
                // Create instance of the provider
                CSharpCodeProvider provider = new CSharpCodeProvider();

                using (StringWriter sw = new StringWriter(sb))
                {
                    IndentedTextWriter tw = new IndentedTextWriter(sw, "    ");

                    // Generate source code using the code provider.
                    provider.GenerateCodeFromCompileUnit(compileUnit, tw,
                        new CodeGeneratorOptions());

                    // Close the output file.
                    tw.Close();
                }
            }
            catch (Exception ex)
            {

            }
            return sb.ToString();
        }

        public static CodeCompileUnit GetSourceGraph(/*List<string> tokens*/)
        {
            CodeCompileUnit compileUnit = new CodeCompileUnit();

            try
            {
                // Namespace for the unit 
                CodeNamespace samples = new CodeNamespace("Samples");

                // Add namespace
                samples.Imports.Add(new CodeNamespaceImport("System"));

                #region Product class

                CodeTypeDeclaration product = new CodeTypeDeclaration("Product");

                CodeMemberField namePropertyField; CodeMemberProperty nameProperty;
                BuildProperty("System.String", "Name", out namePropertyField, out nameProperty);
                product.Members.Add(namePropertyField);
                product.Members.Add(nameProperty);

                CodeMemberField pricePropertyField; CodeMemberProperty priceProperty;
                BuildProperty("System.Single", "Price", out pricePropertyField, out priceProperty);
                product.Members.Add(pricePropertyField);
                product.Members.Add(priceProperty);

                samples.Types.Add(product);

                #endregion Product class

                #region Program class

                // Declare a class
                CodeTypeDeclaration program = new CodeTypeDeclaration("Program");

                // Here we need only one method, containing the logic given by the user
                CodeMemberMethod method = new CodeMemberMethod()
                {
                    Name = "Filter",
                    ReturnType = new CodeTypeReference("System.string"),
                    Attributes = MemberAttributes.Public | MemberAttributes.Final
                };
                method.Parameters.Add(new CodeParameterDeclarationExpression("Product", "product"));

                /*
                 CodeExpression condition = new CodeBinaryOperatorExpression(
    new CodeBinaryOperatorExpression(
        new CodeBinaryOperatorExpression(new CodeVariableReferenceExpression("x"), 
        CodeBinaryOperatorType.ValueEquality, new CodePrimitiveExpression(1)),
        CodeBinaryOperatorType.BooleanAnd,
        new CodeBinaryOperatorExpression(new CodeVariableReferenceExpression("y"), CodeBinaryOperatorType.ValueEquality, new CodePrimitiveExpression("test"))),
    CodeBinaryOperatorType.BooleanOr,
    new CodeMethodInvokeExpression(new CodeMethodReferenceExpression(new CodeVariableReferenceExpression("str"), "Contains"), new CodePrimitiveExpression("test")));
                 
IF(Product.Price > 30 AND Product.Price < 60 AND Product.Name = 'VisualCode')
{
	= Concat(Product.Name, " is priced at ", Product.Price, " per unit");
}*/

                CodeExpression condition = new CodeBinaryOperatorExpression(
                    new CodeBinaryOperatorExpression(
                        new CodeBinaryOperatorExpression(
                            new CodeArgumentReferenceExpression("product.Price"),
                        CodeBinaryOperatorType.GreaterThan,
                        new CodePrimitiveExpression(30)),
                        CodeBinaryOperatorType.BooleanAnd,
                        new CodeBinaryOperatorExpression(
                            new CodeArgumentReferenceExpression("product.Price"),
                        CodeBinaryOperatorType.LessThan,
                        new CodePrimitiveExpression(30))),
                        CodeBinaryOperatorType.BooleanAnd,
                    new CodeBinaryOperatorExpression(
                        new CodeArgumentReferenceExpression("product.Name"),
                        CodeBinaryOperatorType.ValueEquality,
                        new CodePrimitiveExpression("VisualCode"))
                    );

                CodeArrayCreateExpression ca1 = new CodeArrayCreateExpression(
                    new CodeTypeReference("System.String"), 
                    new CodeMethodInvokeExpression(new CodeMethodReferenceExpression(new CodeArgumentReferenceExpression("product.Name"), "ToString")),
                    new CodePrimitiveExpression(" is priced at "),
                    new CodeMethodInvokeExpression(new CodeMethodReferenceExpression(new CodeArgumentReferenceExpression("product.Price"), "ToString")),
                    new CodePrimitiveExpression(" per unit")
                    );

                CodeConditionStatement ifCondition = new CodeConditionStatement(condition, 
                    new CodeStatement[] { new CodeMethodReturnStatement(new CodeMethodInvokeExpression(
                        new CodeTypeReferenceExpression("System.String"), "Concat", ca1)) });
                method.Statements.Add(ifCondition);

                // Add one executable statement to the entry function
                CodeMethodInvokeExpression cs1 = new CodeMethodInvokeExpression(
                    new CodeTypeReferenceExpression("Console"),
                    "WriteLine", new CodePrimitiveExpression("Hello World!"));
                method.Statements.Add(cs1);

                CodeMethodInvokeExpression cs2 = new CodeMethodInvokeExpression(
                    new CodeTypeReferenceExpression("Console"), "ReadLine");
                method.Statements.Add(cs2);

                // Wire the instances to complete the CodeDOM source graph
                program.Members.Add(method);
                samples.Types.Add(program);

                #endregion Program class


                compileUnit.Namespaces.Add(samples);
            }
            catch (Exception ex)
            {

            }

            return compileUnit;
        }

        public static void BuildProperty(string type, string name, out CodeMemberField propertyField, out CodeMemberProperty property)
        {
            if (string.IsNullOrEmpty(type) || string.IsNullOrEmpty(name))
            {
                propertyField = null;
                property = null;
                return;
            }
            string fieldName = $"_{name.ToLower()}";
            propertyField = new CodeMemberField(type, fieldName);
            property = new CodeMemberProperty()
            {
                Name = name,
                Type = new CodeTypeReference(type),
                Attributes = MemberAttributes.Public | MemberAttributes.Final
            };
            property.GetStatements.Add(new CodeMethodReturnStatement(
                new CodeFieldReferenceExpression(new CodeThisReferenceExpression(), fieldName)));
            property.SetStatements.Add(new CodeAssignStatement(
                new CodeFieldReferenceExpression(
                    new CodeThisReferenceExpression(), fieldName),
                new CodePropertySetValueReferenceExpression()));
        }

        public static List<string> ProcessQueryFile()
        {
            string[] lines = File.ReadAllLines(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"input\Query1.lql"));

            int errorPosition;
            List<string> tokens = new List<string>();
            for (int i = 0; i < lines.Length; i++)
            {
                var tns = GetTokens(lines[i], out errorPosition);
                if (tns == null)
                {
                    $"Invalid token in line {i + 1} at position {errorPosition}".LogError();
                    break;
                }
                tokens.AddRange(tns);
            }
            return tokens;
        }

        // parser
        public static List<string> GetTokens(string inputQuery, out int errorPosition)
        {
            errorPosition = -1;
            List<string> tokens = new List<string>();

            string currWord = string.Empty;
            for (int i = 0; i < inputQuery.Length; i++)
            {
                var curCharStr = inputQuery[i].ToString();
                if (curCharStr.IsWhiteSpace())
                {
                    continue;
                }
                else if (curCharStr.IsAlphaNumericOrOperator())
                {
                    currWord = curCharStr;

                    i++;
                    if (i < inputQuery.Length)
                    {
                        curCharStr = inputQuery[i].ToString();
                        while (curCharStr.IsAlphaNumericOrOperator())
                        {
                            currWord += curCharStr;

                            i++;
                            if (i < inputQuery.Length)
                                curCharStr = inputQuery[i].ToString();
                            else
                                break;
                        }
                        if (i < inputQuery.Length)
                            i--;
                    }
                    tokens.Add(currWord);
                }
                else if (curCharStr.IsPunctuations())
                {
                    tokens.Add(curCharStr);
                }
                else if (curCharStr.IsQuotes())
                {
                    currWord = curCharStr;

                    i++;
                    curCharStr = inputQuery[i].ToString();
                    while (!curCharStr.IsQuotes())
                    {
                        currWord += curCharStr;

                        i++;
                        curCharStr = inputQuery[i].ToString();
                    }
                    currWord += curCharStr;
                    tokens.Add(currWord);
                }
                else
                {
                    errorPosition = i;
                    return null;
                }
            }

            return tokens;
        }
    }
}
