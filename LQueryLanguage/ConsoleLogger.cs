﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LQueryLanguage
{
    enum LogSeverity
    {
        Info,
        Warning,
        Error
    }
    public static class ConsoleLogger
    {
        public static void LogInfo(this string message)
        {
            Write(message, LogSeverity.Info);
        }
        public static void LogWarning(this string message)
        {
            Write(message, LogSeverity.Warning);
        }
        public static void LogError(this string message)
        {
            Write(message, LogSeverity.Error);
        }
        static void Write(string message, LogSeverity severity)
        {
            ConsoleColor severityColor;
            switch (severity)
            {
                case LogSeverity.Warning:
                    severityColor = ConsoleColor.Yellow;
                    break;
                case LogSeverity.Error:
                    severityColor = ConsoleColor.Red;
                    break;
                case LogSeverity.Info:
                    severityColor = ConsoleColor.Green;
                    break;
                default:
                    severityColor = ConsoleColor.Gray;
                    break;
            }
            ConsoleColor c = Console.ForegroundColor;
            Console.ForegroundColor = severityColor;
            Console.WriteLine(message);
            Console.ForegroundColor = c;
        }
    }
}
