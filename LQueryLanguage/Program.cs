﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LQueryLanguage
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                DSLTokenizer.Process();

                //DataStore store = new DataStore();
                //store.GetProducts();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.ReadLine();
        }
    }
}
